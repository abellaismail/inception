
DOCKER_COMPOSE_FILE=srcs/docker-compose.yml

up:
	docker compose -f $(DOCKER_COMPOSE_FILE) up -d

down:
	docker compose -f $(DOCKER_COMPOSE_FILE) down 

clean:
	docker compose -f $(DOCKER_COMPOSE_FILE) down -v

destroy_all:
	docker stop $$(docker ps -qa)
	docker rm $$(docker ps -qa)
	docker rmi -f $$(docker images -qa)
	docker volume rm $$(docker volume ls -q)
	docker network rm $$(docker network ls -q)

re: clean up
