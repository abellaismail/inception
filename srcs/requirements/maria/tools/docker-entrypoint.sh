#!/bin/sh

install() {

echo "installing db ..."
mariadb-install-db --user=mysql --basedir=/usr --datadir=/var/lib/mysql \
										--rpm --auth-root-authentication-method=normal \
										--skip-test-db \
										--skip-log-bin \
										--expire-logs-days=0

echo "Running mariadbd ..."
mariadbd  --user=root &

MARIADB_PID=$!

until mariadb -u root -e "CREATE DATABASE IF NOT EXISTS $MARIADB_DB" 2>&1 > /dev/null
do
	sleep 1
done

echo "creating db..."
cat > init.sql << EOF
CREATE USER IF NOT EXISTS ${MARIADB_USER}@'%' IDENTIFIED BY '${MARIADB_PASSWORD}';
GRANT ALL PRIVILEGES ON ${MARIADB_DB}.* TO '${MARIADB_USER}'@'%';
FLUSH PRIVILEGES;
EOF
mariadb -u root < init.sql

echo "successfully launched..."
kill -INT $MARIADB_PID
wait $MARIADB_PID

}

if [ ! -f "/var/lib/mysql/$MARIADB_DB" ] ; then 
	install
fi


mariadbd --user=root
