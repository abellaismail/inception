#!/bin/sh

create_config() {
wp config create --skip-check --dbname="$MARIADB_DB" --dbuser="$MARIADB_USER" --dbpass="$MARIADB_PASSWORD" --dbhost="$MARIADB_HOST" --dbprefix="$WP_PREFIX" --allow-root 
}

cd /var/www/wp/html/
cp -rf /app/wp/* /var/www/wp/html/
if ! wp core --allow-root is-installed ; then
	echo "creating wp-config.php ..."
	create_config
	
	if ! wp db check --allow-root ; then
		exit 1
	fi

	echo "installing wp ..."
	wp core install --url="$DOMAIN" --allow-root --title="$WP_TITLE" --admin_user="$WP_ADMIN" --admin_password="$WP_ADMIN_PASSWORD" --admin_email="$WP_EMAIL"

	echo "installing plugins ..."
	wp plugin install --activate --allow-root redis-cache

	wp config set WP_SITEURL "'https://$DOMAIN'" --raw --allow-root
	wp config set WP_HOME "'https://$DOMAIN'" --raw --allow-root

	wp config set WP_REDIS_HOST "'$REDIS_HOST'" --raw --allow-root
	wp config set WP_REDIS_PORT 6379 --raw --allow-root
	wp config set WP_CACHE true --raw --allow-root
	wp redis update-dropin --allow-root

fi

chmod -R 777 .

php-fpm7
