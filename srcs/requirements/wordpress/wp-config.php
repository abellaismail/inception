<?php
define( 'DB_NAME', 'wp_db' );

define( 'DB_USER', 'wp_user' );

define( 'DB_PASSWORD', 'wp_password' );

define( 'DB_HOST', 'db' );

define( 'DB_CHARSET', 'utf8' );

define( 'DB_COLLATE', '' );

define( 'WP_HOME', 'https://iait-bel.42.fr' );
define( 'WP_SITEURL', 'https://iait-bel.42.fr' );

// define('WP_REDIS_PORT', 6379);
define('WP_REDIS_HOST', 'redis');
define('WP_CACHE_KEY_SALT', 'H1KAO39122');
define('WP_CACHE', true);

define('AUTH_KEY',         '51Nx*4}MS8;kJt9r;S3`I0j`CT-r|_W4<!FMcsmu~#_I10uo*)%<u$A.X3S#-N?%');
define('SECURE_AUTH_KEY',  '+xrNncE[:2gf^=4?<2V|,{eTD^(h3^Q.yh<]V+v~~S8:Gota[3i5;gv9$z d>{:Y');
define('LOGGED_IN_KEY',    '#,f<B?7#<Y^r0BtH-o^WgRs `z<xSt>|I]u2mdnivko$y>;9[:ft|rWLeiBgJos`');
define('NONCE_KEY',        '(-Nc&Epb&z7F<VE|+3tv}p`8~MY,8/tCL26& E=BOi%?J;9wmisO?+|^&+7mb0N>');
define('AUTH_SALT',        'j2_g-/!/v7+c4Q[gU-1/|iAE/yQZ8(vd/cu5P5:1u8U<2H.-wyC-V>LF)#e]b. q');
define('SECURE_AUTH_SALT', 'V#j${#M+&@[kKZ(<41*j%p;(a-J^#m`E(h_00txk`IV-/GDG9NrnYaDm;-/$KM!U');
define('LOGGED_IN_SALT',   '(K&??OQ{Z[ M+t& +?a`<)q8+k19yI.B09l{`Q&;D-r;A;Lu<qwFo^v/Pb*vaZw/');
define('NONCE_SALT',       's;f7Y1_2lk3)+f_w||Zc#RX]WX=&|P4*o|#H|41e?RDcjL{Vk,Z4z=X`G!LQXg9M');

$table_prefix = 'wp_';

define( 'WP_DEBUG', false );

if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

require_once ABSPATH . 'wp-settings.php';
