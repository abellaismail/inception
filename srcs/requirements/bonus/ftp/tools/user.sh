#!/bin/sh

addgroup sftp
adduser "$FTP_USER" -G sftp
echo "${FTP_USER}:${FTP_PASS}" | chpasswd
